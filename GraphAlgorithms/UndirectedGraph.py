import networkx as nx
import matplotlib.pyplot as plt
import copy


class UndirectedGraph:
    def __init__(self, vertex_count, from_list, to_list, cost_list):
        self.IJ, self.H, self.L = [], [], []
        self.cost = []
        self.free = -1
        self.IJ = from_list + to_list[::-1]
        self.H = [-1 for i in range(vertex_count)]
        if len(cost_list) == 0:
            self.cost = [1 for k in range(len(self.IJ))]
        else:
            self.cost = list(cost_list)
        for k in range(len(self.IJ)):
            p = self.IJ[k]
            self.L.append(self.H[p])
            self.H[p] = k

    @staticmethod
    def graphFromCopy(graph):
        if isinstance(graph, UndirectedGraph):
            g = copy.deepcopy(graph)
            return g

    def getVertexCount(self):
        return len(self.H)

    def addVertex(self):
        self.H.append(-1)

    def addEdge(self, tail, head, cost):
        if self.getVertexCount() == 0:
            raise Exception('The Graph is empty!')
        if tail > self.getVertexCount() - 1 or head > self.getVertexCount() - 1:
            raise Exception('Egde parameters are incorrect!')
        if self.free == -1:
            for i in range(len(self.L)):
                if self.L[i] > len(self.L) / 2 - 1:
                    self.L[i] += 2
            for i in range(len(self.H)):
                if self.H[i] > len(self.L) / 2 - 1:
                    self.H[i] += 2
            self.L.insert(int(len(self.L) / 2), self.H[head])
            self.L.insert(int(len(self.L) / 2), self.H[tail])
            self.H[tail] = int(len(self.IJ) / 2)
            self.H[head] = int(len(self.IJ) / 2) + 1
            self.IJ.insert(int(len(self.IJ) / 2), head)
            self.IJ.insert(int(len(self.IJ) / 2), tail)
            self.cost.append(cost)
        else:
            q = self.free
            self.free = self.L[self.free]
            self.L[q] = self.H[tail]
            self.H[tail] = q
            self.IJ[q] = tail
            self.IJ[len(self.IJ) - 1 - q] = head
            self.cost[q] = cost

    def removeEdgeByVertices(self, tail, head):
        self.removeEdgeByIndex(self.indexOfEdge(tail, head))

    def removeEdgeByIndex(self, ind):
        index = ind
        if len(self.IJ) - 1 - ind < ind:
            index = len(self.IJ) - 1 - ind
        if index in self.H:
            self.H[self.H.index(index)] = self.L[index]
        else:
            self.L[self.L.index(index)] = self.L[index]
        self.IJ[index] = -1
        self.L[index] = self.free
        self.free = index
        index = len(self.IJ) - 1 - index
        if index in self.H:
            self.H[self.H.index(index)] = self.L[index]
        else:
            self.L[self.L.index(index)] = self.L[index]
        self.IJ[index] = -1

    def indexOfEdge(self, tail, head):
        for k in range(len(self.IJ)):
            if self.IJ[k] == tail and self.IJ[len(self.IJ) - 1 - k] == head \
                    or self.IJ[k] == head and self.IJ[len(self.IJ) - 1 - k] == tail:
                return k
        return -1

    def __print__(self):
        graph = nx.Graph()
        for i in range(len(self.H)):
            graph.add_node(i)
            q = self.H[i]
            while q != -1:
                graph.add_edge(self.IJ[q], self.IJ[len(self.IJ) - 1 - q],
                               weight=self.cost[min(q, len(self.IJ) - 1 - q)])
                q = self.L[q]
        return graph

    @staticmethod
    def printGraphs(*args):
        for i in range(len(args)):
            gr = args[i].__print__()
            plt.figure(i)
            pos = nx.circular_layout(gr)
            edge_labels = dict([((u, v,), d['weight'])
                                for u, v, d in gr.edges(data=True)])
            nx.draw(gr, pos, with_labels=True, node_color='#A0CBE2')
            nx.draw_networkx_edge_labels(gr, pos, edge_labels=edge_labels)
        plt.show()

    '''filename.txt - находится в том же каталоге, что и main
    '''

    @staticmethod
    def graphFromFile(filename):
        i, j, cost = [], [], []
        with open(filename, 'r') as reader:
            n, m = map(int, reader.readline().split())
            for k in range(m):
                w = list(map(int, reader.readline().split()))
                i.append(int(w[0]))
                j.append(int(w[1]))
                cost.append(int(w[2]))
        return UndirectedGraph(n, i, j, cost)
