from GraphAlgorithms.DirectedGraph import DirectedGraph as Dg
from math import inf


def bfs(graph, s):
    if isinstance(graph, Dg) and isinstance(s, int):
        n = graph.getVertexCount()
        P = [-2 for i in range(n)]  # номера дуг входящих в вершину i, для корня - -1, для недостижимой вершины - -2
        P[s] = -1
        R = [n for i in range(n)]  # расстояния от s до i
        R[s] = 0  # расстояние от s до себя == 0
        Q = [0 for i in range(n)]  # помеченные, но непросмотренные вершины
        Q[0] = s
        r, w = 0, 1  # в листе Q: r указывает на 1ый элемент в очереди, w - на первое свободное место

        while r < w:
            i = Q[r]
            r += 1
            k = graph.H[i]
            while k != -1:
                j = graph.J[k]
                if R[j] == n:
                    R[j] = R[i] + 1
                    P[j] = k
                    Q[w] = j
                    w += 1
                k = graph.L[k]

        from_e, to_e, cost_e = [], [], []
        for a in P:
            if a > -1:
                from_e.append(graph.I[a])
                to_e.append(graph.J[a])
                cost_e.append(graph.cost[a])
        return Dg(n, from_e, to_e, cost_e)


def bellman_ford(graph, s):
    if isinstance(graph, Dg) and isinstance(s, int):
        n = graph.getVertexCount()
        P = [-2 for i in range(n)]  # номера дуг входящих в вершину i, для корня - -1, для недостижимой вершины - -2
        P[s] = -1
        R = [inf for i in range(n)]  # расстояния от s до i
        R[s] = 0  # расстояние от s до себя == 0
        Q = [-2 for i in range(n)]  # помеченные, но непросмотренные вершины
        Q[s] = -1
        h_Q, t_Q = s, s  # в листе Q: h - начало очереди, t - конец очереди
        while h_Q != -1:
            i = h_Q
            h_Q = Q[h_Q]
            Q[i] = -2
            k = graph.H[i]
            while k != -1:
                j = graph.J[k]
                rj = R[j]
                if R[i] + graph.cost[k] < rj:
                    R[j] = R[i] + graph.cost[k]
                    P[j] = k
                    if Q[j] == -2:
                        if rj == inf:
                            if h_Q != -1:
                                Q[t_Q] = j
                            else:
                                h_Q = j
                            t_Q = j
                            Q[j] = -1
                        else:
                            Q[j] = h_Q
                            if h_Q == -1:
                                t_Q = j
                            h_Q = j
                k = graph.L[k]

        from_e, to_e, cost_e = [], [], []
        for a in P:
            if a > -1:
                from_e.append(graph.I[a])
                to_e.append(graph.J[a])
                cost_e.append(graph.cost[a])
        return Dg(n, from_e, to_e, cost_e)


def Dijkstra(graph, s):
    def GET(k):
        if k == -1:
            return -1
        if len(B[k]) == 0:
            return -1
        return B[k].pop()

    def INSERT(i, k):
        B[k].append(i)

    def REMOVE(i, k):
        B[k].remove(i)

    if isinstance(graph, Dg) and isinstance(graph, Dg):
        n = graph.getVertexCount()
        P = [-2 for i in range(n)]  # номера дуг входящих в вершину i, для корня - -1, для недостижимой вершины - -2
        P[s] = -1
        R = [inf for i in range(n)]  # расстояния от s до i
        R[s] = 0  # расстояние от s до себя == 0
        M = n * (n - 1)
        B = [[] for i in range(M)]  # B - черпаки, их nC == n*(n-1) так как максимальная блина дуги == n-1
        INSERT(s, 0)
        for b in range(M):
            i = None
            while i != -1:
                i = GET(b)
                k = graph.H[i]
                while k != -1:
                    j = graph.J[k]
                    rj = R[j]
                    if R[i] + graph.cost[k] < rj:
                        R[j] = R[i] + graph.cost[k]
                        P[j] = k
                        if rj != inf:
                            REMOVE(j, rj)
                        INSERT(j, R[j])
                    k = graph.L[k]


        from_e, to_e, cost_e = [], [], []
        for a in P:
            if a > -1:
                from_e.append(graph.I[a])
                to_e.append(graph.J[a])
                cost_e.append(graph.cost[a])
        return Dg(n, from_e, to_e, cost_e)
