from GraphAlgorithms.UndirectedGraph import UndirectedGraph as Ug


def __findConnectedComponent__(graph):
    if isinstance(graph, Ug):
        Hn, K = list(graph.H), [-1 for i in range(graph.getVertexCount())]
        S = [-1 for i in range(graph.getVertexCount())]
        w = 0
        x = -1
        for i0 in range(graph.getVertexCount()):
            if K[i0] != -1:
                continue
            x += 1
            i = i0
            while True:
                K[i] = x
                k = Hn[i]
                while k != -1:
                    j = graph.IJ[len(graph.IJ) - 1 - k]
                    if (K[j] == -1):
                        break
                    k = graph.L[k]
                if k != -1:
                    Hn[i] = graph.L[k]
                    S[w] = i
                    w += 1
                    i = j
                else:
                    if w == 0:
                        break
                    w -= 1
                    i = S[w]
        return K


def componentCount(graph):
    return max(__findConnectedComponent__(graph)) + 1
