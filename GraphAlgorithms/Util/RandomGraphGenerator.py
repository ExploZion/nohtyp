from random import randint
import os

print('Введите количество вершин графа: ', end='')
n = int(input())
assert n > 1
print('Введите количество дуг/ребер графа: ', end='')
m = int(input())
assert 0 < m <= n * (n - 1) / 2
print('Ориентированный граф - 1, Неориентированный граф - 2: ', end='')
type = int(input())
print('Длины дуг/ребер: единичные - 1, положительные - 2, любые - 3: ', end='')
weight = int(input())
assert type == 1 or type == 2
k = 0
edges = []
cost = []
while k < m:
    i = randint(0, n - 1)
    j = randint(0, n - 1)
    if i == j:
        continue
    if [i, j] not in edges:
        edges.append([i, j])
        if weight == 1:
            cost.append(1)
        elif weight == 2:
            cost.append(randint(1, n - 1))
        else:
            cost.append(randint(-1, n - 1))
        k += 1
with open(os.getcwd() + '/../_GraphInput', 'w') as writer:
    writer.write(str(n) + ' ' + str(m) + '\n')
    i = 0
    for e in edges:
        writer.write(str(e[0]) + ' ' + str(e[1]) + ' ' + str(cost[i]) + '\n')
        i += 1
