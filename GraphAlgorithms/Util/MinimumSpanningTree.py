from GraphAlgorithms.UndirectedGraph import UndirectedGraph


def __sort__(graph):
    for i in range(1, len(graph.cost)):
        while i > 0 and graph.cost[i] < graph.cost[i - 1]:
            graph.cost[i], graph.cost[i - 1] = graph.cost[i - 1], graph.cost[i]
            graph.IJ[i], graph.IJ[i - 1] = graph.IJ[i - 1], graph.IJ[i]
            graph.IJ[len(graph.IJ) - i - 1], graph.IJ[len(graph.IJ) - i] = \
                graph.IJ[len(graph.IJ) - i], graph.IJ[len(graph.IJ) - i - 1]
            i -= 1


def __kruskals_algorithm__(graph):
    if isinstance(graph, UndirectedGraph):

        def find(i):
            return M[i]

        def union(mi, mj):
            if X[mi] < X[mj]:
                k = mi
                l = mj
            else:
                k = mj
                l = mi
            i = H[k]
            while L[i] != -1:
                M[i] = l
                i = L[i]
            M[i] = l
            L[i] = H[l]
            H[l] = H[k]
            X[l] += X[k]

        n = graph.getVertexCount()
        M, H, L, X = [], [], [], []
        K = [0 for i in range(n - 1)]
        k, i = 0, 0
        while k < n:
            H.append(i)
            L.append(-1)
            X.append(1)
            M.append(k)
            k += 1
            i += 1

        __sort__(graph)
        w = 0
        k = 0
        while k < len(graph.IJ) / 2 and w < n - 1:
            mi = find(graph.IJ[k])
            mj = find(graph.IJ[len(graph.IJ) - k - 1])
            if mi != mj:
                K[w] = k
                w += 1
                union(mi, mj)
            k += 1
        return K


def minimumSpanningTree(graph):
    if isinstance(graph, UndirectedGraph):
        tree = __kruskals_algorithm__(graph)
        __sort__(graph)
        from_e, to_e, cost_e = [], [], []
        for i in range(len(tree)):
            from_e.append(graph.IJ[tree[i]])
            to_e.append(graph.IJ[len(graph.IJ) - tree[i] - 1])
            cost_e.append(graph.cost[min(tree[i], len(graph.IJ) - tree[i] - 1)])
        return UndirectedGraph(graph.getVertexCount(), from_e, to_e, cost_e)
