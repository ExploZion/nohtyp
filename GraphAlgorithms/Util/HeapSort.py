from GraphAlgorithms.Heap import Heap


def TreeSort(array):
    if isinstance(array, list):
        heap = Heap(array)
        n = len(array)
        while heap.size > 0:
            heap.array[0], heap.array[heap.size - 1] = heap.array[heap.size - 1], heap.array[0]
            heap.size -= 1
            heap.rem_n(0)
        return heap.array