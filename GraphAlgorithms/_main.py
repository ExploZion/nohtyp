from GraphAlgorithms.UndirectedGraph import UndirectedGraph as Ug
from GraphAlgorithms.DirectedGraph import DirectedGraph as Dg
from GraphAlgorithms.Util.Component import componentCount, __findConnectedComponent__
from GraphAlgorithms.Util.MinimumSpanningTree import minimumSpanningTree
from GraphAlgorithms.Util.ShortestPath import *
from GraphAlgorithms.Util.HeapSort import *
import random

# # Components
# g = Ug.graphFromFile('_test for components')
# # g.addEdge(0, 4, 1)
# # g.addVertex()
# print(componentCount(g))
# Ug.printGraphs(g)


# # Kruskal
# g = Ug.graphFromFile('_test for kruskal')
# gk = MinimumSpanningTree(g)
# Ug.printGraphs(g, gk)


# # BFS
# g = Dg.graphFromFile('_test for bfs')
# g0 = bfs(g, 0)
# g1 = bfs(g, 1)
# g2 = bfs(g, 2)
# Dg.printGraphs(g, g0, g1, g2)


# # Bellman-Ford
# g = Dg.graphFromFile('_test for bellman-ford')
# g0 = bellman_ford(g, 0)
# g1 = bellman_ford(g, 1)
# g5 = bellman_ford(g ,5)
# Dg.printGraphs(g, g0, g1, g5)


# # Dijkstra’s algorithm
# g = Dg.graphFromFile('_test for Dijkstra')
# g1 = Dijkstra(g, 1)
# g3 = Dijkstra(g, 3)
# g6 = Dijkstra(g, 6)
# Dg.printGraphs(g, g1, g3, g6)


# # Heap
# array = []
# while len(array) < 40:
#     e = random.randint(0,100)
#     if e not in array:
#         array.append(e)
# print(array)
# TreeSort(array)
# print(array)
