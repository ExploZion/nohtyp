import networkx as nx
import matplotlib.pyplot as plt

class Heap:
    def __init__(self, array):
        if isinstance(array, list):
            self.size = len(array)
            self.array = array
            k = (self.size - 1) / 2
            while k >= 0:
                self.rem_n(k)
                k -= 1

    def rem_n(self, k0):
        k = int(k0)
        while k < (self.size - 1) / 2:
            k1 = int(2 * k + 1)
            k2 = int(k1 + 1)
            if k2 < self.size and self.array[k2] < self.array[k1]:
                k1 = k2
            if self.array[k] < self.array[k1]:
                break
            self.array[k], self.array[k1] = self.array[k1], self.array[k]
            k = k1

    def rem_v(self, k0):
        k = k0
        while k > 0:
            k1 = int((k - 1) / 2)
            if self.array[k1] < self.array[k]:
                break
            self.array[k], self.array[k1] = self.array[k1], self.array[k]
            k = k1

    def get_min(self):
        min = self.array[0]
        self.array[0] = self.array[self.size - 1]
        self.size -= 1
        self.rem_n(0)
        return min

    def add(self, e):
        self.array.append(e)
        self.size += 1
        self.rem_v(self.size - 1)

    def remove(self, k0):
        a = self.array[k0]
        self.array[k0] = self.array[self.size - 1]
        self.size -= 1
        if self.array[k0] > a:
            self.rem_n(k0)
        else:
            self.rem_v(k0)