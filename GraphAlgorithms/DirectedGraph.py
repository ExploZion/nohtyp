import networkx as nx
import matplotlib.pyplot as plt
import copy


class DirectedGraph:
    def __init__(self, vertex_count, from_list, to_list, cost_list):
        self.I, self.J, self.H, self.L = [], [], [], []
        self.cost = []
        self.free = -1
        for i in range(vertex_count):
            self.H.append(-1)
        self.I = list(from_list)
        self.J = list(to_list)
        if len(cost_list) == 0:
            for k in range(len(self.I)):
                self.cost.append(1)
        else:
            self.cost = list(cost_list)
        for k in range(len(from_list)):
            p = self.I[k]
            self.L.append(self.H[p])
            self.H[p] = k

    @staticmethod
    def graphFromCopy(digraph):
        g = copy.deepcopy(digraph)
        return g

    def getVertexCount(self):
        return len(self.H)

    def addVertex(self):
        self.H.append(-1)

    def addEdge(self, tail, head, cost=1):
        if self.getVertexCount() == 0:
            raise Exception('The Graph is empty!')
        if tail > self.getVertexCount() - 1 or head > self.getVertexCount() - 1:
            raise Exception('Egde parameters are incorrect!')
        if self.free == -1:
            self.L.append(self.H[tail])
            self.H[tail] = len(self.I)
            self.I.append(tail)
            self.J.append(head)
            self.cost.append(cost)
        else:
            q = self.free
            self.free = self.L[self.free]
            self.L[q] = self.H[tail]
            self.H[tail] = q
            self.I = tail
            self.J = head
            self.cost = cost

    def removeEdgeByVertices(self, tail, head):
        self.removeEdgeByIndex(self.indexOfEdge(tail, head))

    def removeEdgeByIndex(self, index):
        if index in self.H:
            self.H[self.H.index(index)] = self.L[index]
        else:
            self.L[self.L.index(index)] = self.L[index]
        self.I[index] = -1
        self.J[index] = -1
        self.L[index] = self.free
        self.free = index

    def indexOfEdge(self, tail, head):
        for k in range(len(self.I)):
            if self.I[k] == tail and self.J[k] == head:
                return k
        return -1

    def __print__(self):
        graph = nx.DiGraph()
        for i in range(len(self.H)):
            graph.add_node(i)
            q = self.H[i]
            while q != -1:
                graph.add_edge(self.I[q], self.J[q], weight=self.cost[q])
                q = self.L[q]
        return graph

    @staticmethod
    def printGraphs(*args):
        for i in range(len(args)):
            gr = args[i].__print__()
            plt.figure(i)
            pos = nx.circular_layout(gr)
            edge_labels = dict([((u, v,), d['weight'])
                                for u, v, d in gr.edges(data=True)])
            nx.draw(gr, pos, with_labels=True, node_color='#A0CBE2')
            nx.draw_networkx_edge_labels(gr, pos, edge_labels=edge_labels)
        plt.show()

    ''' filename.txt - находится в том же каталоге, что и main
    '''

    @staticmethod
    def graphFromFile(filename):
        i, j, cost = [], [], []
        with open(filename, 'r') as reader:
            n, m = map(int, reader.readline().split())
            for k in range(m):
                w = reader.readline().split()
                i.append(int(w[0]))
                j.append(int(w[1]))
                cost.append(int(w[2]))
        return DirectedGraph(n, i, j, cost)
