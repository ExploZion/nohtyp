from SortAlgorithms.Base import Base


class Insertion(Base):
    def sort(a):
        n = len(a)
        for i in range(1, n):
            while i > 0 and a[i] < a[i - 1]:
                a[i], a[i - 1] = a[i - 1], a[i]
                i -= 1

    @staticmethod
    def show(a):
        super(Insertion, Insertion).show(a)

    @staticmethod
    def is_sorted(a):
        super(Insertion, Insertion).is_sorted(a)
