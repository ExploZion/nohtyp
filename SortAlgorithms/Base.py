class Base:
    @staticmethod
    def sort(a):
        pass

    @staticmethod
    def show(a):
        for i in range(len(a)):
            print(str(a[i]) + ' ', end='')
        print()

    @staticmethod
    def is_sorted(a):
        for i in range(len(a)):
            if a[i] < a[i - 1]:
                return False
        return True
