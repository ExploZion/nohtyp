from SortAlgorithms.Base import Base

class Selection(Base):
    @staticmethod
    def sort(a):
        n = len(a)
        for i in range(n):
            min = i
            for j in range(i, n):
                if a[j] < a[min]:
                    min = j
            a[i], a[min] = a[min], a[i]

    @staticmethod
    def show(a):
        super(Selection, Selection).show(a)

    @staticmethod
    def is_sorted(a):
        super(Selection, Selection).is_sorted(a)
