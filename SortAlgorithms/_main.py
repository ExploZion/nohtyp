from SortAlgorithms.Insertion import Insertion
from random import *

a = [randint(1,10) for i in range(10)]
print(Insertion.is_sorted(a))
Insertion.show(a)
Insertion.sort(a)
print(str(Insertion.is_sorted(a)))
Insertion.show(a)